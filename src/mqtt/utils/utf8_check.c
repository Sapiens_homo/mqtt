
#include "../../../include/mqtt/utils.h"

int UTF8_VALID ( char *restrict src ){
	register int n = 1;
	register unsigned char tmp = 0;
	register int i;

	tmp = *src;
	while( tmp & 0x80 ){
		n++;
		tmp <<= 1;
	}

	for ( i = n; --i; ){
		if ( ( *src & 0xC0 ) != 0x80 ){
			n = 0;
			break;
		}
	}
	
	return n;
}
