
#include "../../../include/mqtt/utils.h"
#include <errno.h>

inline __attribute__((always_inline))
	uint32_t MQTT_decode_byte ( char *restrict src, enum _MQTT_DATA_TYPE_ type ) {
		register uint32_t ret = 0U;

		register size_t i;

		switch ( type ){
			case BYTE:
			case WORD:
			case DWORD:
				for ( i = 0; i < type; i++ ){
					ret <<= CHAR_BIT;
					ret |= *( src + i );
				}
				break;
			case UTF8:
				if ( ( i = UTF8_VALID( src ) ) ){
					switch ( i ){
						case 1:
							ret = *src;
							break;
						case 2:
							ret = *src & 0x1F;
							break;
						case 3:
							ret = *src &0x0F;
							break;
						case 4:
							ret = *src & 0x07;
							break;
						default:
							break;
					}

					for ( ; --i; ){
						ret <<= 6;
						ret |= *( src + i ) & 0x3F;
					}
				}
				break;
			case VARBYTE:
				i = 0;
				do{
					ret <<= 7;
					ret |= *( src + i ) & 0x7F;
				}while( i < 4 && *( src + i++ ) & 0x80 );
				if( *( src + i ) & 0x80 ){
					ret = 0;
					errno = EDOM;
				}
				break;
			default:
				break;
		}

		return ret;
}
