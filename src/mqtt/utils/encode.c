
#include "../../../include/mqtt/utils.h"
#include <string.h>
#include <limits.h>

inline __attribute__((always_inline))
	MQTT_INT MQTT_encode_byte ( uint32_t val, enum _MQTT_DATA_TYPE_ type ){
#if __BYTE_ORDER__ == __ORDER_PDP_ENDIAN__
#error We DON'T support PDP endian
#endif
	register MQTT_INT ret = {
		.len = 0,
		.value = NULL
	};

	register size_t i;
	register size_t j;
	register uint32_t tmp;

	switch ( type ){
		case BYTE:
		case WORD:
		case DWORD:
			ret.len = type;
			while ( !ret.value ){
				ret.value = malloc( type );
			}
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
			for ( i = type, j = 0; i; --i, j++ ){
				*( ret.value + j ) = *(unsigned char *)((void*)&val + i );
			}
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
			memcpy( ret.value, &val, type );
#endif
			break;
		case UTF8:
		/*
		 * MIN	| MAX	| 1	| 2	| 3	| 4
		 * 0000	| 007F	| 0
		 * 0080	| 07FF	| 110	| 10
		 * 0800	| FFFF	| 1110	| 10	| 10
		 *010000| 10FFFF| 11110	| 10	| 10	| 10
		 */
			if ( val < 0x0080U ){
				ret.len = 1;
			}else if ( val < 0x0800U ){
				ret.len = 2;
			}else if ( val < 0x010000U ){
				ret.len = 3;
			}else if ( val < 0x110000U ){
				ret.len = 4;
			}else {
				break;
			}
			ret.value = malloc( ret.len );
			if( ret.len - 1 ){
				for ( i = ret.len; --i; ){
					*( ret.value + i ) = 0x80U | ( val & 0x3FU );
					val >>= 6;
				}
				*( ret.value ) = ( ( ( 1 << ret.len ) - 1 ) << ( CHAR_BIT - ret.len ) ) |
					( val & ( ( 1 << ( CHAR_BIT - ret.len - 1 ) ) - 1 ) );
			}else{
				*ret.value = val & 0x7FU;
			}
			break;
		case VARBYTE:
			type = 0;
			tmp = val;
			do {
				tmp >>= 7;
				type ++;
			}while( tmp );
			ret.len = type;
			ret.value = malloc( type );
			for ( i = type; i; --i ){
				*( ret.value + i ) = ( val & 0x7FU ) | 0x80U;
				val >>= 7U;
			}
			*( ret.value + ret.len - 1 ) ^= 0x80U;
			break;
		default:
			break;
	}

	return ret;
}
