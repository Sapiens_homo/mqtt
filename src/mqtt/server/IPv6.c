
#include "../../../include/mqtt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#include <sys/syscall.h>
#include <unistd.h>

#include <errno.h>

#include <pthread.h>

void *MQTT_server_IPv6 ( void *restrict info ){
	register int *status = malloc(sizeof(*status));
	register int *cnt = NULL;
	register int fd;

	pthread_t threads[MAX_THREAD];
	register size_t thread_used;

	int *ret;

	struct sockaddr_in6 addr;

	*status = EXIT_SUCCESS;
	thread_used = 0;

	fd = socket( AF_INET6, SOCK_STREAM, 0 );
	if ( fd == -1 ){
		fprintf( stderr, "IPv6 Socket error\n" );
		*status = EXIT_FAILURE;
		return status;
	}

	memset ( &addr, 0, sizeof (addr) );
	addr.sin6_family = AF_INET6;
	addr.sin6_port = htons( *(int *)info );
	addr.sin6_addr = in6addr_any;

	if ( bind ( fd, (void *)&addr, sizeof( addr ) ) == -1 ){
		fprintf ( stderr, "Bind: %s\n", strerror( errno ) );
		*status = EXIT_FAILURE;
		goto EXIT;
	}
	if ( listen ( fd, 32 ) == -1 ){
		fprintf ( stderr, "Listen failed\n" );
		*status = EXIT_FAILURE;
		goto EXIT;
	}

	while ( !*status &&
		( cnt = malloc( sizeof( *cnt ) ),
		*cnt = accept ( fd, NULL, NULL ) >= 0 ) ){
		if ( thread_used >= MAX_THREAD ){
			while ( thread_used -- ){
				pthread_join( threads[thread_used], (void *)&ret );
				*status |= *ret;
				free( ret );
			}
		}
		pthread_create ( threads + thread_used++, NULL, MQTT_handler, cnt );
	}

	free( cnt );

EXIT:

	while ( thread_used-- ){
		pthread_cancel( threads[thread_used] );
	}
	close( fd );

	return status;
}
