
CC ?= gcc
C_FLAGS = -Wall -std=gnu99 -march=native
C_DEBUG_FLAGS = -g -D DBG
C_FILES = $(shell find src -name "*.c")
OBJ = $(patsubst %.c, %.o, $(C_FILES))
OBJ_DBG = $(patsubst %.c, %.o.dbg, $(C_FILES))
OBJ_FILE = $(shell find . -name "*.o*")
OUTPUT_FILE = $(shell find . -name "broker*")

.PHONY: all debug clean

all: broker

debug: broker.dbg

clean:
	-rm $(OBJ_FILE) $(OUTPUT_FILE)

%.o: %.c
	$(CC) $(C_FLAGS) -c -o $@ $<

%.o.dbg: %.c
	$(CC) $(C_FLAGS) $(C_DEBUG_FLAGS) -c -o $@ $<

broker: $(OBJ)
	$(CC) $(C_FLAGS) -o $@ $^

broker.dbg: $(OBJ_DBG)
	$(CC) $(C_FLAGS) $(C_DEBUG_FLAGS) -o $@ $^
