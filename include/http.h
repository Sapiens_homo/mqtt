
#ifndef __HTTP_H__
#define __HTTP_H__

#include "server_struct.h"

extern void *HTTP_server_IPv4 ( void * );
extern void *HTTP_server_IPv6 ( void * );
extern void *HTTP_server_UNIX ( void * );
extern void *HTTP_handler ( void * );

#endif
