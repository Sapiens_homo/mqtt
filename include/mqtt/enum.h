
#ifndef __MQTT_ENUM_H__
#define __MQTT_ENUM_H__

enum __MQTT_CONTROL_PACKET_TYPE__ {
	MQTT_CONNECT	=	0x1,
	MQTT_CONNACK	=	0x2,
	MQTT_PUBLISH	=	0x3,
	MQTT_PUBACK	=	0x4,
	MQTT_PUBREC	=	0x5,
	MQTT_PUBREL	=	0x6,
	MQTT_PUBCOMP	=	0x7,
	MQTT_SUBSCRIBE	=	0x8,
	MQTT_SUBACK	=	0x9,
	MQTT_UNSUBSCRIBE=	0xA,
	MQTT_UNSUBACK	=	0xB,
	MQTT_PINGREQ	=	0xC,
	MQTT_PINGRESP	=	0xD,
	MQTT_DISCONNECT	=	0xE,
	MQTT_AUTH	=	0xF
};

enum __MQTT_PROPERTY_ID__ {
	MQTT_Payload_Format_Indicator	=	0x01,
	MQTT_Message_Expiry_Interval	=	0x02,
	MQTT_Cotent_Type		=	0x03,
	MQTT_Respponse_Topic		=	0x08,
	MQTT_Correlation_Data		=	0x09,
	MQTT_Subscription_Identifier	=	0x0B,
	MQTT_Session_Exiry_Interval	=	0x11,
	MQTT_Assigned_Client_Identifier	=	0x12,
	MQTT_Server_Keep_Alive		=	0x13,
	MQTT_Authentication_Method	=	0x15,
	MQTT_Authentication_Data	=	0x16,
	MQTT_Request_Problem_Information=	0x17,
	MQTT_Will_Delay_Interval	=	0x18,
	MQTT_Request_Response_Information=	0x19,
	MQTT_Response_Information	=	0x1A,
	MQTT_Server_Reference		=	0x1C,
	MQTT_Reason_String		=	0x1F,
	MQTT_Receive_Maximum		=	0x21,
	MQTT_Topic_Alias_Maximum	=	0x22,
	MQTT_Topic_Alias		=	0x23,
	MQTT_Maximum_QoS		=	0x24,
	MQTT_Retain_Available		=	0x25,
	MQTT_User_Property		=	0x26,
	MQTT_Maximum_Packet_Size	=	0x27,
	MQTT_Wildcard_Subscription_Available=	0x28,
	MQTT_Subscription_Identifier_Available=	0x29,
	MQTT_Shared_Subscription_Available=	0x2A,
};

#endif
