
#ifndef __MQTT_STRUCTURE_H__
#define __MQTT_STRUCTURE_H__

enum _MQTT_DATA_TYPE_ {
	BYTE	=	1,
	WORD	=	2,
	DWORD	=	4,
	UTF8	=	6,
	VARBYTE	=	8,
};

struct _MQTT_INT_ {
	unsigned char len;
	unsigned char *value;
};

typedef struct _MQTT_INT_ MQTT_INT;

#endif
