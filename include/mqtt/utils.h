
#ifndef __MQTT_UTILS_H__
#define __MQTT_UTILS_H__

#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include "structure.h"

extern inline MQTT_INT MQTT_encode_byte ( uint32_t, enum _MQTT_DATA_TYPE_ );
extern inline uint32_t MQTT_decode_byte ( char *, enum _MQTT_DATA_TYPE_ );

extern int UTF8_VALID ( char * );

#endif
