
#ifndef __MQTT_H__
#define __MQTT_H__

#include "mqtt/structure.h"
#include "mqtt/utils.h"

#include "server_struct.h"

#define MAX_THREAD 4

extern void *MQTT_server_IPv4 ( void * ); /* int port */
extern void *MQTT_server_IPv6 ( void * ); /* int port */
extern void *MQTT_server_UNIX ( void * ); /* char *path */
extern void *MQTT_handler ( void * ); /* int socket */

#endif
